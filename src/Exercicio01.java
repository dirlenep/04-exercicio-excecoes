import java.util.InputMismatchException;
import java.util.Scanner;

public class Exercicio01 {
	public static void main(String[] args) {
	
		int numero1 = 0;
		int numero2 = 0;
		int result = 0;
		
		Scanner teclado = new Scanner(System.in);

		try {
			System.out.println("Digite o primeiro n�mero");
			numero1 = teclado.nextInt();
			System.out.println("Digite o segundo n�mero");
			numero2 = teclado.nextInt();
			result = numero1 / numero2;
			System.out.println(result);
		} catch (InputMismatchException e) {
			System.out.println("Valor informado n�o � um n�mero");
		} catch (ArithmeticException e) {
			System.out.println("N�o � poss�vel divis�o por zero");
		}
	}
}
