import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Exercicio02 {

	public static void main(String[] args) {
	    ArrayList<Integer> vetor = new ArrayList<Integer>(10);

	    Scanner teclado = new Scanner(System.in);
	    int numero;
	    
	    try {
		    do {
		    	System.out.println("Digite um n�mero");
		    	numero = teclado.nextInt();
		    	vetor.add(numero);
		    } while (numero != 0);
		    if (vetor.size() > 10) {
		    	System.out.println(vetor);
		    	throw new ArrayIndexOutOfBoundsException();
		    } else {
		    	System.out.println(vetor);
		    }
		} catch (InputMismatchException e) {
			System.out.println("S� � poss�vel inserir n�meros inteiros");
		} catch (ArrayIndexOutOfBoundsException e) {
	    	System.out.println("S� � poss�vel colocar 10 n�meros no array");
	    }
	}
}
