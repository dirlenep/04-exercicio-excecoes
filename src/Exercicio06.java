import java.util.InputMismatchException;
import java.util.Scanner;

public class Exercicio06 {

	public static void main(String[] args) {
		
		Scanner teclado = new Scanner(System.in);
		try {
			System.out.println("Eu sei dividir!");
			System.out.println("Informe o primeiro valor ");
			int x = teclado.nextInt();
			System.out.println("Informe o segundo valor ");
			int y = teclado.nextInt();
			double r = (x / y);
			System.out.println("O resultado da divis�o � " + r);
		} catch (InputMismatchException e) {
			System.out.println("Valor informado n�o � um n�mero inteiro");
		} catch (ArithmeticException e) {
			System.out.println("N�o � poss�vel divis�o por zero");
		}

	}

}