package Exercicio03;

public class Login {
	private String usuario;
	private String senha;
	
	public Login(String usuario, String senha) {
		this.usuario = usuario;
		this.senha = senha;
	}
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public boolean fazerLogin(String usuario, String senha) {
		boolean logou = false;
		try {
			if (usuario != this.usuario) {
				throw new Exception("Usu�rio Incorreto!");
			} else if (senha != this.senha) {
				throw new Exception("Senha Incorreta!");
			} else {
				System.out.println("Usu�rio Logado");
				logou = true;
			}
		} catch (Exception e) {
			System.out.println("Erro" + e);
		}
		return logou;
	}

}
